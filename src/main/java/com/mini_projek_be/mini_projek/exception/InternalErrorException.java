package com.mini_projek_be.mini_projek.exception;

public class InternalErrorException extends RuntimeException{

    public InternalErrorException(String message) {
        super(message);
    }
}
