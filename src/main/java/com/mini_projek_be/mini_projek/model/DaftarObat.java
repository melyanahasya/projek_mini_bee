package com.mini_projek_be.mini_projek.model;

import javax.persistence.*;

@Entity
@Table(name = "daftar_obat")
public class DaftarObat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama_obat")
    private String NamaObat;

    @Column(name = "stok")
    private Long Stok;

    public DaftarObat() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaObat() {
        return NamaObat;
    }

    public void setNamaObat(String namaObat) {
        NamaObat = namaObat;
    }

    public Long getStok() {
        return Stok;
    }

    public void setStok(Long stok) {
        Stok = stok;
    }
}
