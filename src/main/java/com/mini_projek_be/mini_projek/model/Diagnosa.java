package com.mini_projek_be.mini_projek.model;

import com.mini_projek_be.mini_projek.modelRegister.Register;

import javax.persistence.*;

@Entity
@Table(name = "diagnosa")
public class Diagnosa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama_diagnosa")
    private String namaDiagnosa;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Register userId;


    public Diagnosa() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaDiagnosa() {
        return namaDiagnosa;
    }

    public void setNamaDiagnosa(String namaDiagnosa) {
        this.namaDiagnosa = namaDiagnosa;
    }

    public Register getUserId() {
        return userId;
    }

    public void setUserId(Register userId) {
        this.userId = userId;
    }
}
