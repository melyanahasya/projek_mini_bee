package com.mini_projek_be.mini_projek.model;

import com.mini_projek_be.mini_projek.modelRegister.Register;

import javax.persistence.*;

@Entity
@Table(name = "penanganan_pertama")
public class PenangananPertama {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama_penanganan")
        private String penangananPertama;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Register userId;

    public PenangananPertama() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPenangananPertama() {
        return penangananPertama;
    }

    public void setPenangananPertama(String penangananPertama) {
        this.penangananPertama = penangananPertama;
    }

    public Register getUserId() {
        return userId;
    }

    public void setUserId(Register userId) {
        this.userId = userId;
    }
}
