package com.mini_projek_be.mini_projek.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mini_projek_be.mini_projek.modelRegister.Register;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "periksa_pasien")
public class PeriksaPasien {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Register userId;

    @OneToOne
    @JoinColumn(name = "nama_pasien")
    private Pasien namaPasien;

    @Column(name = "status_pasien")
    private String statusPasien;

    @Column(name = "keluhan")
    private String keluhan;

    @Column(name = "status")
    private String status;

    @ManyToOne
    @JoinColumn(name = "penyakit_id")
    private Diagnosa penyakitId;

    @ManyToOne
    @JoinColumn(name = "penanganan_id")
    private PenangananPertama penangananId;

    @ManyToOne
    @JoinColumn(name = "tindakan_id")
    private Tindakan tindakanId;

    @JsonFormat(pattern = "dd-MM-yyyy")
    @CreationTimestamp
    @Column(name = "tanggal")
    private Date tanggal;

    @JsonFormat(pattern = "HH:mm:ss")
    @Column(name = "waktu", updatable = false)
    private Date waktu;



    public PeriksaPasien() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Register getUserId() {
        return userId;
    }

    public void setUserId(Register userId) {
        this.userId = userId;
    }

    public Pasien getNamaPasien() {
        return namaPasien;
    }

    public void setNamaPasien(Pasien namaPasien) {
        this.namaPasien = namaPasien;
    }

    public String getStatusPasien() {
        return statusPasien;
    }

    public void setStatusPasien(String statusPasien) {
        this.statusPasien = statusPasien;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Diagnosa getPenyakitId() {
        return penyakitId;
    }

    public void setPenyakitId(Diagnosa penyakitId) {
        this.penyakitId = penyakitId;
    }

    public PenangananPertama getPenangananId() {
        return penangananId;
    }

    public void setPenangananId(PenangananPertama penangananId) {
        this.penangananId = penangananId;
    }

    public Tindakan getTindakanId() {
        return tindakanId;
    }

    public void setTindakanId(Tindakan tindakanId) {
        this.tindakanId = tindakanId;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public Date getWaktu() {
        return waktu;
    }

    public void setWaktu(Date waktu) {
        this.waktu = waktu;
    }

    public String getKeluhan() {
        return keluhan;
    }

    public void setKeluhan(String keluhan) {
        this.keluhan = keluhan;
    }
}
