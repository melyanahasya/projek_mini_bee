package com.mini_projek_be.mini_projek.model;

import com.mini_projek_be.mini_projek.modelRegister.Register;

import javax.persistence.*;

@Entity
@Table(name = "tindakan")
public class Tindakan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama_tindakan")
    private String namaTindakan;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Register userId;

    public Tindakan() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaTindakan() {
        return namaTindakan;
    }

    public void setNamaTindakan(String namaTindakan) {
        this.namaTindakan = namaTindakan;
    }

    public Register getUserId() {
        return userId;
    }

    public void setUserId(Register userId) {
        this.userId = userId;
    }
}
