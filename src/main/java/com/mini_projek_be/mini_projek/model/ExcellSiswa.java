package com.mini_projek_be.mini_projek.model;


import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ExcellSiswa {

    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    static String[] HEADERs = {"No ", "nama_siswa","kelas", "tempat","tanggal_lahir","alamat"};

    static String SHEET = "Sheet1";

    public static boolean hasExcelFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }

    public static ByteArrayInputStream siswaToExcell(List<Pasien> pasiens){
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();){
            Sheet sheet = workbook.createSheet(SHEET);

            Row headerRow = sheet.createRow(0);

            for (int col = 0; col < HEADERs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(HEADERs[col]);
            }

            int rowIdx = 1;
            for (Pasien pasien : pasiens){
                Row row = sheet.createRow(rowIdx++);
                row.createCell(0).setCellValue(pasien.getId());
                row.createCell(1).setCellValue(pasien.getUsername());
                row.createCell(2).setCellValue(pasien.getKelas());
                row.createCell(3).setCellValue(pasien.getTempat());
                row.createCell(4).setCellValue(pasien.getTanggalLahir());
                row.createCell(5).setCellValue(pasien.getAlamat());
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }catch (IOException e){
            throw new RuntimeException("fail import data to excell");
        }
    }


    public static List<Pasien> excelToSiswa(InputStream is) {

        try {
            Workbook workbook = new XSSFWorkbook(is);

            Sheet sheet = workbook.getSheet(SHEET);
            Iterator<Row> rows = sheet.iterator();

            List<Pasien> pasiens = new ArrayList<Pasien>();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();

                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();

                Pasien pasien = new Pasien();

                int cellIdx = 0;
                while (cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();

                    switch (cellIdx) {
                        case 0:
                            pasien.setUsername(currentCell.getStringCellValue());
                            break;
                        case 1:
                            pasien.setKelas(currentCell.getStringCellValue());
                            break;
                        case 2:
                            pasien.setTempat(currentCell.getStringCellValue());
                            break;
                        case 3:
                            pasien.setTanggalLahir(currentCell.getStringCellValue());
                            break;
                        case 4:
                            pasien.setAlamat(currentCell.getStringCellValue());
                            break;


                        default:
                            break;
                    }
                    pasiens.add(pasien);
                    cellIdx++;

                }
            }
            workbook.close();
            return pasiens;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
        }

    }

}
