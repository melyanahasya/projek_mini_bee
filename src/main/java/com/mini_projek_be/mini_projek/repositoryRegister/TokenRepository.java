package com.mini_projek_be.mini_projek.repositoryRegister;

import com.mini_projek_be.mini_projek.modelRegister.TemporaryToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenRepository extends JpaRepository<TemporaryToken, Long> {

    Optional<TemporaryToken> findByToken(String token);

    Optional<TemporaryToken> findByRegisterId(long RegisterId);
}
