package com.mini_projek_be.mini_projek.configuration;

import com.mini_projek_be.mini_projek.jwt.AccesDenied;
import com.mini_projek_be.mini_projek.jwt.JwtAuthTokenFilter;
import com.mini_projek_be.mini_projek.jwt.UnauthorizedError;
import com.mini_projek_be.mini_projek.serviceImplRegister.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AccesDenied accesDenied;

    @Autowired
    private UnauthorizedError unauthorizedError;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    private static final String[] AUTH_WHITLIST = {
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            // -- Swagger UI v3 (OpenAPI)
            "/v3/api-docs/**",
            "/swagger-ui/**",
            "/authentication/**",
            "/tindakan/**",
            "/periksa_pasien/**",
            "/daftar_siswa/**",
            "/daftar_guru/**",
            "/daftar_karyawan/**",
            "/daftar_obat/**",
            "/register/**",
            "/penanganan_pertama/**",
            "/periksa_pasien/**",
            "/api/guruExcell",
            "/api/siswaExcell",
            "/api/karyawanExcell",
            "/api/excel_periksa_pasien",
            "/api/download/template_guru",
            "/api/download/template_siswa",
            "/api/download/template_karyawan",
            "/api//upload/siswa",
            "/api//upload/guru",
            "/api//upload/karyawan"
    };

    @Bean
    public JwtAuthTokenFilter authTokenFilter() {
        return new JwtAuthTokenFilter();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .exceptionHandling().authenticationEntryPoint(unauthorizedError).and()
                .exceptionHandling().accessDeniedHandler(accesDenied).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
//            .antMatchers(AUTH_WHITLIST).permitAll()
                .anyRequest().permitAll();
        http.addFilterBefore(authTokenFilter(), UsernamePasswordAuthenticationFilter.class);
    }

}
