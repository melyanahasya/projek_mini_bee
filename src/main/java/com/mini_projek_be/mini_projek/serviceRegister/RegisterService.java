package com.mini_projek_be.mini_projek.serviceRegister;

import com.mini_projek_be.mini_projek.modelRegister.Login;
import com.mini_projek_be.mini_projek.modelRegister.Register;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface RegisterService {

    Register registrasi(Register register);

    Map<String, Object> login(Login login);

    Register getById(Long id);

    Register update(Long id, Register register);

    Register updatePassword(Long id, Register register);

    Register updateFoto(Long id, Register register, MultipartFile multipartFile);

    Map<String, Boolean> deleteRegister(Long id);

    Page<Register> getAllRegister(String query, Long page);
}
