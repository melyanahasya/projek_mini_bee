package com.mini_projek_be.mini_projek.serviceImpl;

import com.mini_projek_be.mini_projek.dto.PeriksaPasienDTO;
import com.mini_projek_be.mini_projek.dto.TanganiDTO;
import com.mini_projek_be.mini_projek.exception.NotFoundException;
import com.mini_projek_be.mini_projek.model.PeriksaPasien;
import com.mini_projek_be.mini_projek.repository.*;
import com.mini_projek_be.mini_projek.service.PeriksaPasienService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PeriksaPasienServiceImpl implements PeriksaPasienService {

    @Autowired
    PeriksaPasienRepository periksaPasienRepository;

    @Autowired
    DiagnosaRepository diagnosaRepository;

    @Autowired
    PenangananPertamaRepository penangananRepository;

    @Autowired
    TindakanRepository tindakanRepository;

    @Autowired
    PasienRepository pasienRepository;

    @Override
    public Page<PeriksaPasien> allPasien(String query, Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return periksaPasienRepository.allPasien(query, pageable);
    }

    @Override
    public PeriksaPasien addPasien(PeriksaPasienDTO periksaPasien) {
        PeriksaPasien periksaPasien1 = new PeriksaPasien();
        periksaPasien1.setKeluhan(periksaPasien.getKeluhan());
        periksaPasien1.setStatusPasien(periksaPasien.getStatusPasien());
        periksaPasien1.setStatus("belum ditangani");
        periksaPasien1.setNamaPasien(pasienRepository.findById(periksaPasien.getNamaPasien()).orElseThrow(() -> new NotFoundException("id not found")));
        return periksaPasienRepository.save(periksaPasien1);
    }

    @Override
    public PeriksaPasien editTangani(Long id, TanganiDTO tanganiDto) {
        PeriksaPasien periksaPasien = periksaPasienRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        periksaPasien.setStatus("sudah ditangani");
        periksaPasien.setPenyakitId(diagnosaRepository.findById(tanganiDto.getPanyakitId()).orElseThrow(() -> new NotFoundException("id not found")));
        periksaPasien.setPenangananId(penangananRepository.findById(tanganiDto.getPenangananId()).orElseThrow(() -> new NotFoundException("id not found")));
        periksaPasien.setTindakanId(tindakanRepository.findById(tanganiDto.getTindakanId()).orElseThrow(() -> new NotFoundException("id not found")));
        periksaPasien.setKeluhan(tanganiDto.getKeluhan());
        return periksaPasienRepository.save(periksaPasien);
    }

    @Override
    public PeriksaPasien getIdd(Long id) {
        var periksaPasien = periksaPasienRepository.findById(id).get();
        return periksaPasienRepository.save(periksaPasien);
    }

    @Override
    public List<PeriksaPasien> filterTanggal(String query, String tanggal) {
        return periksaPasienRepository.filterTanggal(query, tanggal);
    }


}
