package com.mini_projek_be.mini_projek.serviceImpl;

import com.mini_projek_be.mini_projek.dto.PasienDTO;
import com.mini_projek_be.mini_projek.dto.PasienSiswaDTO;
import com.mini_projek_be.mini_projek.exception.NotFoundException;
import com.mini_projek_be.mini_projek.model.Pasien;
import com.mini_projek_be.mini_projek.modelRegister.Register;
import com.mini_projek_be.mini_projek.repository.PasienRepository;
import com.mini_projek_be.mini_projek.service.PasienService;
import com.mini_projek_be.mini_projek.serviceRegister.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PasienServiceImpl implements PasienService {


    @Autowired
    PasienRepository pasienRepository;

    @Autowired
    RegisterService registerService;


    @Override
    public List<Pasien> allGuru(Long userId) {
        return pasienRepository.getAllGuru(userId);
    }

    @Override
    public List<Pasien> allSiswa(Long userId) {
        return pasienRepository.getAllSiswa(userId);
    }

    @Override
    public List<Pasien> allKaryawan(Long userId) {
        return pasienRepository.getAllKaryawan(userId);
    }

    @Override
    public Page<Pasien> allSiswaId(Long page, Long userId) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page),5);
        return pasienRepository.getAllUserId(pageable, userId);
    }

    private static final int HOUR = 3600 * 1000;

    @Override
    public Pasien addPasien(PasienDTO pasienDto) {
        Pasien pasien = new Pasien();
        Register register = registerService.getById(pasienDto.getUserId());
        pasien.setSiswaId(register);
        pasien.setUsername(pasienDto.getUsername());
        pasien.setJabatan(pasienDto.getJabatan());
        pasien.setStatus("guru");
        pasien.setTempat(pasienDto.getTempat());
        pasien.setTanggalLahir(pasienDto.getTanggalLahir());
        pasien.setAlamat(pasienDto.getAlamat());
        return pasienRepository.save(pasien);
    }

    @Override
    public Pasien addSiswa(PasienSiswaDTO pasienDto) {
        Pasien pasien = new Pasien();
        Register register = registerService.getById(pasienDto.getUserId());
        pasien.setSiswaId(register);
        pasien.setUsername(pasienDto.getUsername());
        pasien.setJabatan(pasienDto.getJabatan());
        pasien.setStatus("siswa");
        pasien.setTempat(pasienDto.getTempat());
        pasien.setTanggalLahir(pasienDto.getTanggalLahir());
        pasien.setAlamat(pasienDto.getAlamat());
        pasien.setKelas(pasienDto.getKelas());
        return pasienRepository.save(pasien);
    }

    @Override
    public Pasien addKaryawan(PasienDTO pasienDto) {
        Pasien pasien = new Pasien();
        Register register = registerService.getById(pasienDto.getUserId());
        pasien.setSiswaId(register);
        pasien.setUsername(pasienDto.getUsername());
        pasien.setJabatan(pasienDto.getJabatan());
        pasien.setStatus("karyawan");
        pasien.setTempat(pasienDto.getTempat());
        pasien.setTanggalLahir(pasienDto.getTanggalLahir());
        pasien.setAlamat(pasienDto.getAlamat());
        return pasienRepository.save(pasien);
    }

    @Override
    public Pasien editPasien(Long id, PasienDTO pasienDto) {
        Pasien pasien = pasienRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        pasien.setUsername(pasienDto.getUsername());
        pasien.setTanggalLahir(pasienDto.getTanggalLahir());
        pasien.setJabatan(pasienDto.getJabatan());
        pasien.setTempat(pasienDto.getTempat());
        pasien.setAlamat(pasienDto.getAlamat());
        return pasienRepository.save(pasien);
    }

    @Override
    public Pasien editSiswa(Long id, PasienSiswaDTO pasienDto) {
        Pasien pasien = pasienRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        pasien.setUsername(pasienDto.getUsername());
        pasien.setTanggalLahir(pasienDto.getTanggalLahir());
        pasien.setJabatan(pasienDto.getJabatan());
        pasien.setTempat(pasienDto.getTempat());
        pasien.setAlamat(pasienDto.getAlamat());
        pasien.setKelas(pasienDto.getKelas());
        return pasienRepository.save(pasien);
    }

    @Override
    public Pasien editKaryawan(Long id, PasienDTO pasienDto) {
        Pasien pasien = pasienRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        pasien.setUsername(pasienDto.getUsername());
        pasien.setTanggalLahir(pasienDto.getTanggalLahir());
        pasien.setJabatan(pasienDto.getJabatan());
        pasien.setTempat(pasienDto.getTempat());
        pasien.setAlamat(pasienDto.getAlamat());
        return pasienRepository.save(pasien);
    }

    @Override
    public Pasien getId(Long id) {
        var karyawan = pasienRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
        return pasienRepository.save(karyawan);
    }


    @Override
    public Map<String, Boolean> deleteById(Long id) {
        try {
            pasienRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Hapus", Boolean.TRUE);
            return res;
        } catch (Exception e) {

            Map<String, Boolean> tes = new HashMap<>();
            tes.put("Hapus", Boolean.FALSE);
            return tes;
        }
    }


}
