package com.mini_projek_be.mini_projek.serviceImpl;

import com.mini_projek_be.mini_projek.dto.DaftarObatDTO;
import com.mini_projek_be.mini_projek.exception.NotFoundException;
import com.mini_projek_be.mini_projek.model.DaftarObat;
import com.mini_projek_be.mini_projek.repository.DaftarObatRepository;
import com.mini_projek_be.mini_projek.service.DaftarObatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class DaftarObatServiceImpl implements DaftarObatService {

    @Autowired
    DaftarObatRepository daftarObatRepository;


    @Override
    public Page<DaftarObat> getAllObat(String query, Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return daftarObatRepository.searchfindAll(query, pageable);
    }

    @Override
    public DaftarObat addObat(DaftarObatDTO daftarObat) {
        DaftarObat daftarObat1 = new DaftarObat();
        daftarObat1.setNamaObat(daftarObat.getNamaObat());
        daftarObat1.setStok(daftarObat.getStok());
        return daftarObatRepository.save(daftarObat1);
    }

    @Override
    public DaftarObat editObat(Long id, DaftarObatDTO daftarObat) {
        DaftarObat daftarObat1 = daftarObatRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        daftarObat1.setNamaObat(daftarObat.getNamaObat());
        daftarObat1.setStok(daftarObat.getStok());
        return daftarObatRepository.save(daftarObat1);
    }

    @Override
    public DaftarObat getIdObat(Long id) {
        return daftarObatRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
    }

    @Override
    public Map<String, Boolean> deleteObat(Long id) {
        try {
            daftarObatRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Hapus", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            return null;
        }
    }
}
