package com.mini_projek_be.mini_projek.serviceImpl;

import com.mini_projek_be.mini_projek.dto.PenangananPertamaDTO;
import com.mini_projek_be.mini_projek.exception.NotFoundException;
import com.mini_projek_be.mini_projek.model.PenangananPertama;
import com.mini_projek_be.mini_projek.repository.PenangananPertamaRepository;
import com.mini_projek_be.mini_projek.service.PenangananPertamaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class PenangananPertamaServiceImpl implements PenangananPertamaService {

    @Autowired
    PenangananPertamaRepository penangananPertamaRepository;

    @Override
    public Page<PenangananPertama> getAllPenanganan(String query, Long page){
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return penangananPertamaRepository.serchfindAll(query, pageable);
    }

    @Override
    public PenangananPertama addPenanganan(PenangananPertamaDTO penanganan){
        PenangananPertama penangananPertama1 = new PenangananPertama();
        penangananPertama1.setPenangananPertama(penanganan.getNamaPenangananPertama());
        return penangananPertamaRepository.save(penangananPertama1);
    }


    @Override
    public PenangananPertama getIdPenanganan(Long id) {
        var penanganan = penangananPertamaRepository.findById(id).get();
        return penangananPertamaRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
    }


    @Override
    public PenangananPertama editPenanganan(Long id, PenangananPertamaDTO penangananPertama) {
        PenangananPertama pertama = penangananPertamaRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
        pertama.setPenangananPertama(penangananPertama.getNamaPenangananPertama());
        return penangananPertamaRepository.save(pertama);
    }

    @Override
    public Map<String, Boolean> deletePenanganan(Long id){
        try {
            penangananPertamaRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Hapus", Boolean.TRUE);
            return res;
        } catch (Exception e){
            return null;
        }
    }

    @Override
    public Page<PenangananPertama> allPenanganan(Long page, Long userId) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return penangananPertamaRepository.getAllPenanganan(pageable, userId);
    }
}
