package com.mini_projek_be.mini_projek.serviceImpl;

import com.mini_projek_be.mini_projek.dto.TindakanDto;
import com.mini_projek_be.mini_projek.exception.NotFoundException;
import com.mini_projek_be.mini_projek.model.Tindakan;
import com.mini_projek_be.mini_projek.modelRegister.Register;
import com.mini_projek_be.mini_projek.repository.TindakanRepository;
import com.mini_projek_be.mini_projek.service.TindakanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class TindakanServiceImpl implements TindakanService {

    @Autowired
    TindakanRepository tindakanRepository;

    @Override
     public Page<Tindakan> getAllTindakan(String query, Long page){
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return tindakanRepository.searchfindAll(query, pageable);
    }


    @Override
    public Tindakan getTindakan(Long id) {
        return tindakanRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
    }


    @Override
    public Tindakan addTindakan(TindakanDto tindakan){
        Tindakan tindakan1 = new Tindakan();
        tindakan1.setNamaTindakan(tindakan.getNamaTindakan());
        return tindakanRepository.save(tindakan1);
    }

    @Override
    public Tindakan editTindakan(Long id, Tindakan tindakan) {
        Tindakan tindakan1 = tindakanRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
        tindakan1.setNamaTindakan(tindakan.getNamaTindakan());
        return tindakanRepository.save(tindakan1);
    }


    @Override
    public Map<String, Boolean> deleteTindakan(Long id){
        try {
            tindakanRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Hapus", Boolean.TRUE);
            return res;
        }catch (Exception e){
            return null;
        }
    }

    @Override
    public Page<Tindakan> allTindakan(Long page, Long userId) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return tindakanRepository.getAllTindakan(pageable, userId);
    }
}
