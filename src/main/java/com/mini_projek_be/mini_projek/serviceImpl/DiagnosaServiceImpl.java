package com.mini_projek_be.mini_projek.serviceImpl;

import com.mini_projek_be.mini_projek.dto.DiagnosaDTO;
import com.mini_projek_be.mini_projek.exception.NotFoundException;
import com.mini_projek_be.mini_projek.model.Diagnosa;
import com.mini_projek_be.mini_projek.model.Tindakan;
import com.mini_projek_be.mini_projek.repository.DiagnosaRepository;
import com.mini_projek_be.mini_projek.service.DiagnosaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class DiagnosaServiceImpl implements DiagnosaService {
    @Autowired
    DiagnosaRepository diagnosaRepository;

    @Override
    public Page<Diagnosa> getAllDiagnosa(String query, Long page){
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return diagnosaRepository.searchfindAll(query, pageable);
    }

    @Override
    public Diagnosa addDiagnosa(DiagnosaDTO diagnosa){
        Diagnosa diagnosa1 = new Diagnosa();
        diagnosa1.setNamaDiagnosa(diagnosa.getNamaDiagnosa());
        return diagnosaRepository.save(diagnosa1);
    }

    @Override
    public Diagnosa getDiagnosa(Long id) {
        return diagnosaRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
    }

    @Override
    public Diagnosa editDiagnosa(Long id, DiagnosaDTO diagnosa){
        Diagnosa diagnosa1 = diagnosaRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        diagnosa1.setNamaDiagnosa(diagnosa.getNamaDiagnosa());
        return  diagnosaRepository.save(diagnosa1);
    }

    @Override
    public Map<String, Boolean> deleteDiagnosa(Long id){
        try {
            diagnosaRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Hapus", Boolean.TRUE);
            return res;
        } catch (Exception e){
            return null;
        }
    }

    @Override
    public Page<Diagnosa> allDiangnosa(Long page, Long userId) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return diagnosaRepository.getAllDiangnosa(pageable, userId);
    }
}
