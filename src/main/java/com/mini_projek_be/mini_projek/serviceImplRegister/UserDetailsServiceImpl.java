package com.mini_projek_be.mini_projek.serviceImplRegister;

import com.mini_projek_be.mini_projek.modelRegister.Register;
import com.mini_projek_be.mini_projek.modelRegister.UserPrinciple;
import com.mini_projek_be.mini_projek.repositoryRegister.RegisterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private RegisterRepository repository;

    //    membuat token
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        mengecek email
        boolean isEmail = Pattern.compile("^(.+)@(\\S+)$")
                .matcher(username).matches();
        Register user;
        System.out.println("is Email " + isEmail);

//        jika email ada
        if(isEmail) {
            user = repository.findByEmail(username);
        } else { // else username
            user = repository.findByUsername(username);
        }
        return UserPrinciple.build(user);
    }

}
