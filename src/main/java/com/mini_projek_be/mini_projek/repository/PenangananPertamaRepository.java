package com.mini_projek_be.mini_projek.repository;

import com.mini_projek_be.mini_projek.model.PenangananPertama;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PenangananPertamaRepository extends JpaRepository<PenangananPertama, Long> {

    @Query(value = "SELECT * FROM penanganan_pertama  WHERE " +
            "nama_penanganan LIKE CONCAT('%',:query, '%')", nativeQuery = true)
    Page<PenangananPertama> serchfindAll(String query, Pageable pageable);

    @Query(value = "SELECT * FROM penanganan_pertama WHERE user_id = ?1", nativeQuery = true)
    Page<PenangananPertama> getAllPenanganan(Pageable pageable, Long userId);
}