package com.mini_projek_be.mini_projek.repository;

import com.mini_projek_be.mini_projek.model.Diagnosa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DiagnosaRepository extends JpaRepository<Diagnosa, Long> {

    @Query(value = "SELECT * FROM diagnosa WHERE nama_diagnosa LIKE CONCAT('%', ?1, '%')", nativeQuery = true)
    Page<Diagnosa> searchfindAll(String search, Pageable pageable);

    @Query(value = "SELECT * FROM diagnosa WHERE user_id = ?1", nativeQuery = true)
    Page<Diagnosa> getAllDiangnosa(Pageable pageable, Long userId);

}
