package com.mini_projek_be.mini_projek.repository;

import com.mini_projek_be.mini_projek.model.PeriksaPasien;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PeriksaPasienRepository extends JpaRepository<PeriksaPasien, Long> {

//    @Query(value = "SELECT * FROM periksa_pasien  WHERE keluhan LIKE CONCAT('%', ?1, '%')",nativeQuery = true)
//    Page<PeriksaPasien> searchFindAll(String search, Pageable pageable);

    @Query(value = "SELECT * FROM periksa_pasien WHERE nama_pasien LIKE CONCAT ('%',:query,'%')", nativeQuery = true)
    Page<PeriksaPasien> allPasien (String query, Pageable pageable);

    @Query(value = "SELECT * FROM periksa_pasien WHERE tanggal LIKE CONCAT ('%', :query, '%') AND tanggal LIKE CONCAT ('%', :tanggal, '%')", nativeQuery = true)
    List<PeriksaPasien> filterTanggal(String query, String tanggal);

}
