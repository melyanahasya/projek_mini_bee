package com.mini_projek_be.mini_projek.repository;

import com.mini_projek_be.mini_projek.model.DaftarObat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DaftarObatRepository extends JpaRepository<DaftarObat, Long> {

    @Query(value = "SELECT * FROM daftar_obat WHERE nama_obat LIKE CONCAT('%', ?1, '%')", nativeQuery = true)
    Page<DaftarObat> searchfindAll(String search, Pageable pageable);
}
