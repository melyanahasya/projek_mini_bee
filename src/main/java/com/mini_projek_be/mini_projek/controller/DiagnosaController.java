package com.mini_projek_be.mini_projek.controller;

import com.mini_projek_be.mini_projek.dto.DiagnosaDTO;
import com.mini_projek_be.mini_projek.model.Diagnosa;
import com.mini_projek_be.mini_projek.response.CommonResponse;
import com.mini_projek_be.mini_projek.response.ResponHelper;
import com.mini_projek_be.mini_projek.service.DiagnosaService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/diagnosa")
@CrossOrigin("http://localhost:3004")
public class DiagnosaController {

    @Autowired
    DiagnosaService diagnosaService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/all") // untuk mencari semua data
    public CommonResponse<Page<Diagnosa>> getAllDiagnosa(@RequestParam(required = false)String search, @RequestParam(name = "page")Long page) {
        return ResponHelper.ok(diagnosaService.getAllDiagnosa(search == null ? "": search, page));
    }

    @PostMapping // untuk menambahkan data
    public CommonResponse<Diagnosa> addDiagnosa(DiagnosaDTO diagnosa){
        return ResponHelper.ok(diagnosaService.addDiagnosa(diagnosa));
    }

    @DeleteMapping("/{id}") // untuk menghapus data per id
    public  CommonResponse<?> deleteDiagnosa(@PathVariable("id")Long id){
        return ResponHelper.ok(diagnosaService.deleteDiagnosa(id));
    }

    @PutMapping(path = "/{id}") // untuk mengedit data
    public CommonResponse<Diagnosa> editDiagnosa(@PathVariable("id")Long id, @RequestBody DiagnosaDTO diagnosa){
        return ResponHelper.ok(diagnosaService.editDiagnosa(id, diagnosa));
    }

    @GetMapping("/{id}") // untuk mencari data per id
    public CommonResponse<Diagnosa> getDiagnosa(@PathVariable("id")Long id) {
        return ResponHelper.ok(diagnosaService.getDiagnosa(id));
    }

    @GetMapping("/all-diagnosa")// untuk menampilkan semua data
    public CommonResponse<Page<Diagnosa>> allDiagnosa(@RequestParam (name = "userId") Long userId, @RequestParam(name = "page") Long page) {
        return ResponHelper.ok(diagnosaService.allDiangnosa(page, userId));
    }

}
