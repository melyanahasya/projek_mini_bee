package com.mini_projek_be.mini_projek.controller;

import com.mini_projek_be.mini_projek.dto.ExcellDTO;
import com.mini_projek_be.mini_projek.model.ExcellSiswa;
import com.mini_projek_be.mini_projek.service.ExcellSiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/siswaExcell")
public class ExcellSiswaController {

    @Autowired
    ExcellSiswaService excellSiswaService;

    @GetMapping("/download/template_siswa")
    public ResponseEntity<Resource> getFileSiswa() {
        String filename = "siswa.xlsx";
        InputStreamResource file = new InputStreamResource(excellSiswaService.pExcell());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }

//    @GetMapping("/download/template")
//    public ResponseEntity<Resource> getFileTemplate() {
//        String filename = "template.xlsx";
//        InputStreamResource file = new InputStreamResource(templateService.pExcell());
//
//        return ResponseEntity.ok()
//                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
//                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
//                .body(file);
//    }

    @PostMapping(path = "/upload/siswa")
    public ResponseEntity<?> uploadFileSiswa(@RequestPart("file") MultipartFile file, ExcellDTO excellDTO)
    {
        String message = "";
        if (ExcellSiswa.hasExcelFormat(file)) {
            try {
                excellSiswaService.saveSiswa(file);
                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                return ResponseEntity.status(HttpStatus.OK).body(message);
            } catch (Exception e) {
                System.out.println(e);
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
            }
        }
        message = "Please upload an excel file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
    }
}
