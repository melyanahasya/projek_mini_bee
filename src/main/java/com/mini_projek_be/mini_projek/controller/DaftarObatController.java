package com.mini_projek_be.mini_projek.controller;

import com.mini_projek_be.mini_projek.dto.DaftarObatDTO;
import com.mini_projek_be.mini_projek.model.DaftarObat;
import com.mini_projek_be.mini_projek.response.CommonResponse;
import com.mini_projek_be.mini_projek.response.ResponHelper;
import com.mini_projek_be.mini_projek.service.DaftarObatService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/daftar_obat")
@CrossOrigin(origins = "http://localhost:3004")
public class DaftarObatController {

    @Autowired
    DaftarObatService daftarObatService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/all")
    public CommonResponse<Page<DaftarObat>> getAllObat(@RequestParam(required = false)String search, @RequestParam(name = "page")Long page) {
        return ResponHelper.ok(daftarObatService.getAllObat(search == null ? "": search, page));
    }

    @PostMapping
    public CommonResponse<DaftarObat> addObat(DaftarObatDTO daftarObat){
        return ResponHelper.ok(daftarObatService.addObat(daftarObat));
    }

    @DeleteMapping("/{id}")
    public  CommonResponse<?> deleteObat(@PathVariable("id")Long id){
        return ResponHelper.ok(daftarObatService.deleteObat(id));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse<DaftarObat> editObat(@PathVariable("id")Long id, @RequestBody DaftarObatDTO daftarObat){
        return ResponHelper.ok(daftarObatService.editObat(id, daftarObat));
    }

    @GetMapping("/{id}")
    public CommonResponse<DaftarObat> getIdObat(@PathVariable("id")Long id){
        return ResponHelper.ok(daftarObatService.getIdObat(id));
    }
}
