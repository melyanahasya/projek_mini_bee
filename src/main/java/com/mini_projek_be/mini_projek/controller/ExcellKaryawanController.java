package com.mini_projek_be.mini_projek.controller;

import com.mini_projek_be.mini_projek.dto.ExcellDTO;
import com.mini_projek_be.mini_projek.model.ExcellGuru;
import com.mini_projek_be.mini_projek.model.ExcellKaryawan;
import com.mini_projek_be.mini_projek.service.ExcellKaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.service.ResponseMessage;

@RestController
@RequestMapping("/api/karyawanExcell")
public class ExcellKaryawanController {

    @Autowired
    ExcellKaryawanService excellKaryawanService;

    private static final String EXTERNAL_FILE_PATH_KARYAWAN = "contoh-format-karyawan.xlsx";


    @GetMapping("/download/template_karyawan")
    public ResponseEntity<Resource> getFileKaryawan() {
        String filename = "karyawan.xlsx";
        InputStreamResource file = new InputStreamResource(excellKaryawanService.pExcell());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }


    @PostMapping(path = "/upload/guru")
    public ResponseEntity<?> uploadFilekaryawan(@RequestPart("file") MultipartFile file, ExcellDTO excellDTO)
    {
        String message = "";
        if (ExcellGuru.hasExcelFormat(file)) {
            try {
                excellKaryawanService.saveKaryawan(file, excellDTO);
                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                return ResponseEntity.status(HttpStatus.OK).body(message);
            } catch (Exception e) {
                System.out.println(e);
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
            }
        }
        message = "Please upload an excel file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
    }
}
