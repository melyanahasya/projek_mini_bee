package com.mini_projek_be.mini_projek.controller;

import com.mini_projek_be.mini_projek.dto.PeriksaPasienDTO;
import com.mini_projek_be.mini_projek.dto.TanganiDTO;
import com.mini_projek_be.mini_projek.model.PeriksaPasien;
import com.mini_projek_be.mini_projek.response.CommonResponse;
import com.mini_projek_be.mini_projek.response.ResponHelper;
import com.mini_projek_be.mini_projek.service.PeriksaPasienService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("periksa_pasien")
public class PeriksaPasienController {

    @Autowired
    PeriksaPasienService periksaPasienService;

//    @Autowired
//    ExcellTangani excelTangani;


//    @GetMapping("/download")
//    public ResponseEntity<Resource> download_data_pasien(String query, String tanggal) {
//        String filename = "data_pasien.xlsx";
//        InputStreamResource file = new InputStreamResource(excelTangani.loadPasien(query, tanggal));
//
//        return ResponseEntity.ok()
//                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
//                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
//                .body(file);
//    }


    @GetMapping
    public CommonResponse<Page<PeriksaPasien>> allPasien(@RequestParam(name = "query", required = false) String query, @RequestParam(name = "page")Long page){
        return ResponHelper.ok(periksaPasienService.allPasien(query == null ? "": query, page));
    }

    @GetMapping("/filterTanggal")
    public CommonResponse<List<PeriksaPasien>> filterTanggal(@RequestParam(name = "query", required = false) String query, @RequestParam(name = "tanggal", required = false) String tanggal){
        return ResponHelper.ok(periksaPasienService.filterTanggal(query == null ? "": query, query == null ? "": query));
    }

    @PostMapping
    public CommonResponse<PeriksaPasien> addPasien( PeriksaPasienDTO siswa ){
        return ResponHelper.ok(periksaPasienService.addPasien(siswa));
    }

    @PutMapping("/{id}")
    public CommonResponse<PeriksaPasien> editTangani (@PathVariable("id") Long id, TanganiDTO tanganiDto) {
        return ResponHelper.ok(periksaPasienService.editTangani(id, tanganiDto));
    }

    @GetMapping("/{id}")
    public CommonResponse <PeriksaPasien> getId(@PathVariable("id")Long id){
        return ResponHelper.ok( periksaPasienService.getIdd(id));
    }

}
