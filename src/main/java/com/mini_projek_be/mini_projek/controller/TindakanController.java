package com.mini_projek_be.mini_projek.controller;

import com.mini_projek_be.mini_projek.dto.TindakanDto;
import com.mini_projek_be.mini_projek.model.Tindakan;
import com.mini_projek_be.mini_projek.modelRegister.Register;
import com.mini_projek_be.mini_projek.response.CommonResponse;
import com.mini_projek_be.mini_projek.response.ResponHelper;
import com.mini_projek_be.mini_projek.service.TindakanService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tindakan")
@CrossOrigin("http://localhost:3004")
public class TindakanController {

    @Autowired
    TindakanService tindakanService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/all")
    public CommonResponse<Page<Tindakan>> getAllTindakan(@RequestParam (required = false)String search, @RequestParam(name = "page")Long page) {
        return ResponHelper.ok(tindakanService.getAllTindakan(search == null ? "": search, page));
    }

    @PostMapping
    public CommonResponse<Tindakan> addTindakan(TindakanDto tindakanDto){
        return ResponHelper.ok(tindakanService.addTindakan(tindakanDto));
    }

    @DeleteMapping("/{id}")
    public CommonResponse <?> deleteTindakan(@PathVariable("id")Long id) {
        return ResponHelper.ok(tindakanService.deleteTindakan(id));
    }

    @PutMapping("/{id}")
    public CommonResponse<Tindakan> editTindakan(@PathVariable("id") Long id, @RequestBody Tindakan tindakan) {
        return ResponHelper.ok(tindakanService.editTindakan(id, tindakan));
    }

    @GetMapping("/{id}")
    public CommonResponse<Tindakan> getTindakan(@PathVariable("id") Long id) {
        return ResponHelper.ok(tindakanService.getTindakan(id));
    }

    @GetMapping("/all-tindakan")
    public CommonResponse<Page<Tindakan>> allTindakan(@RequestParam (name = "userId") Long userId, @RequestParam(name = "page") Long page) {
        return ResponHelper.ok(tindakanService.allTindakan(page, userId));
    }
}
