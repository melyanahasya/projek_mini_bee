package com.mini_projek_be.mini_projek.controller;

import com.mini_projek_be.mini_projek.dto.PenangananPertamaDTO;
import com.mini_projek_be.mini_projek.model.PenangananPertama;
import com.mini_projek_be.mini_projek.response.CommonResponse;
import com.mini_projek_be.mini_projek.response.ResponHelper;
import com.mini_projek_be.mini_projek.service.PenangananPertamaService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/penanganan_pertama")
@CrossOrigin(origins = "http://localhost:3004")
public class PenangananPertamaController {

    @Autowired
    PenangananPertamaService penangananPertamaService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/all")
    public CommonResponse<Page<PenangananPertama>> getAllPenanganan(@RequestParam(required = false)String search, @RequestParam(name = "page")Long page) {
        return ResponHelper.ok(penangananPertamaService.getAllPenanganan(search == null ? "": search, page));
    }


    @PostMapping
    public CommonResponse<PenangananPertama> addPenanganan(PenangananPertamaDTO penanganan){
        return ResponHelper.ok(penangananPertamaService.addPenanganan(penanganan));
    }

    @DeleteMapping("/{id}")
    public  CommonResponse<?> deletePenanganan(@PathVariable("id")Long id){
        return ResponHelper.ok(penangananPertamaService.deletePenanganan(id));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse<PenangananPertama> editPenanganan(@PathVariable("id")Long id, @RequestBody PenangananPertamaDTO penanganan){
        return ResponHelper.ok(penangananPertamaService.editPenanganan(id, penanganan));
    }

    @GetMapping("/{id}")
    public CommonResponse<PenangananPertama> getIdPenanganan(@PathVariable("id")Long id) {
        return ResponHelper.ok(penangananPertamaService.getIdPenanganan(id));
    }

    @GetMapping("/all-penanganan")
    public CommonResponse<Page<PenangananPertama>> allPenanganan(@RequestParam (name = "userId") Long userId, @RequestParam(name = "page") Long page) {
        return ResponHelper.ok(penangananPertamaService.allPenanganan(page, userId));
    }
}
