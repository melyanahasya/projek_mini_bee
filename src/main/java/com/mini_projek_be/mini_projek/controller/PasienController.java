package com.mini_projek_be.mini_projek.controller;

import com.mini_projek_be.mini_projek.dto.PasienDTO;
import com.mini_projek_be.mini_projek.dto.PasienSiswaDTO;
import com.mini_projek_be.mini_projek.model.Pasien;
import com.mini_projek_be.mini_projek.response.CommonResponse;
import com.mini_projek_be.mini_projek.response.ResponHelper;
import com.mini_projek_be.mini_projek.service.PasienService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pasien")
@CrossOrigin("http://localhost:3004")
public class PasienController {

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    PasienService pasienService;

    @GetMapping("/{id}") // melihat data sesuai id
    public CommonResponse<Pasien> getId (@PathVariable("id") Long id) {
        return ResponHelper.ok(pasienService.getId(id));
    }

    @GetMapping("/all-userId")// untuk menampilkan semua data
    public CommonResponse<Page<Pasien>> allSiswaId(@RequestParam(name = "userId") Long userId, @RequestParam(name = "page") Long page) {
        return ResponHelper.ok(pasienService.allSiswaId(page, userId));
    }

    @GetMapping("/getAll-guru")// untuk menampilkan semua data
    public CommonResponse<List<Pasien>> allGuru(@RequestParam(name = "userId") Long userId) {
        return ResponHelper.ok(pasienService.allGuru(userId));
    }

    @GetMapping("/all-siswa")// untuk menampilkan semua data
    public CommonResponse<List<Pasien>> allSiswa(@RequestParam(name = "userId") Long userId) {
        return ResponHelper.ok(pasienService.allSiswa( userId));
    }

    @GetMapping("/all-karyawan")// untuk menampilkan semua data
    public CommonResponse<List<Pasien>> allKaryawan(@RequestParam(name = "userId") Long userId) {
        return ResponHelper.ok(pasienService.allKaryawan(userId));
    }

    @PostMapping("post-guru") //untuk menambahkan data
    public CommonResponse<Pasien> addPasienGuru (PasienDTO pasienDto) {
        return ResponHelper.ok(pasienService.addPasien(modelMapper.map(pasienDto, PasienDTO.class)));
    }

    @PostMapping("post-siswa") //untuk menambahkan data
    public CommonResponse<Pasien> addPasienSiswa (PasienSiswaDTO pasienSiswaDto) {
        return ResponHelper.ok(pasienService.addSiswa(modelMapper.map(pasienSiswaDto, PasienSiswaDTO.class)));
    }

    @PostMapping("post-karyawan") //untuk menambahkan data
    public CommonResponse<Pasien> addPasienKaryawan (PasienDTO pasienKaryawanDto) {
        return ResponHelper.ok(pasienService.addKaryawan(modelMapper.map(pasienKaryawanDto, PasienDTO.class)));
    }

    @PutMapping("/{id}_guru") //untuk mengedit data per id
    public CommonResponse<Pasien> editGuru (@PathVariable("id") Long id, @RequestBody PasienDTO pasienDto) {
        return ResponHelper.ok(pasienService.editPasien(id, pasienDto));
    }

    @PutMapping("/{id}_siswa") //untuk mengedit data per id
    public CommonResponse<Pasien> editSiswa (@PathVariable("id") Long id, @RequestBody PasienSiswaDTO pasienSiswaDto) {
        return ResponHelper.ok(pasienService.editSiswa(id, pasienSiswaDto));
    }

    @PutMapping("/{id}_karyawan") //untuk mengedit data per id
    public CommonResponse<Pasien> editKaryawan(@PathVariable("id") Long id, @RequestBody PasienDTO pasienKaryawanDto) {
        return ResponHelper.ok(pasienService.editKaryawan(id, pasienKaryawanDto));
    }

    @DeleteMapping("/{id}")// untuk delete per id
    public CommonResponse<?> deleteById (@PathVariable("id") Long id) {
        return ResponHelper.ok(pasienService.deleteById(id));
    }



}
