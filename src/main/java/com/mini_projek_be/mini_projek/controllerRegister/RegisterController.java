package com.mini_projek_be.mini_projek.controllerRegister;

import com.mini_projek_be.mini_projek.modelRegister.Foto;
import com.mini_projek_be.mini_projek.modelRegister.Login;
import com.mini_projek_be.mini_projek.modelRegister.Password;
import com.mini_projek_be.mini_projek.modelRegister.Register;
import com.mini_projek_be.mini_projek.response.CommonResponse;
import com.mini_projek_be.mini_projek.response.ResponHelper;
import com.mini_projek_be.mini_projek.serviceRegister.RegisterService;
import org.hibernate.sql.Update;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RestController
@RequestMapping("/register")
@CrossOrigin(origins = "http://localhost:3004")
public class RegisterController {

    @Autowired
    private RegisterService registerService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/register")
    public CommonResponse<Register> registrasi (@RequestBody Register register) {
        return ResponHelper.ok(registerService.registrasi(register));
    }

    @PostMapping("/login")
    public CommonResponse<Map<String, Object>> login(@RequestBody Login login) {
        return ResponHelper.ok(registerService.login(login));
    }

    @GetMapping("/{id}")
    public CommonResponse<Register> getById(@PathVariable("id") Long id) {
        return ResponHelper.ok(registerService.getById(id));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse<Register> update(@PathVariable("id") Long id,@RequestBody Update update) {
        return ResponHelper.ok(registerService.update(id, modelMapper.map(update , Register.class)));
    }

    @PutMapping(path = "/password/{id}")
    public CommonResponse<Register> updatePassword(@PathVariable("id") Long id,@RequestBody Password password) {
        return ResponHelper.ok(registerService.updatePassword(id, modelMapper.map(password , Register.class)));
    }

    @PutMapping(path = "/foto/{id}", consumes = "multipart/form-data")
    public CommonResponse<Register> updateFoto(@PathVariable("id") Long id, Foto foto, @RequestPart("file") MultipartFile multipartFile) {
        return ResponHelper.ok(registerService.updateFoto(id, modelMapper.map(foto , Register.class) , multipartFile));
    }

    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> deleteRegister(@PathVariable("id") Long id) {
        return ResponHelper.ok(registerService.deleteRegister(id));
    }
    @GetMapping
    public CommonResponse<Page<Register>> getAllResgister(@RequestParam(name = "query", required = false) String query, Long page) {
        return ResponHelper.ok(registerService.getAllRegister(query == null ? "" : query, page));
    }

}
