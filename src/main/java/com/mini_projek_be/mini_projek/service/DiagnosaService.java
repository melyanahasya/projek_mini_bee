package com.mini_projek_be.mini_projek.service;

import com.mini_projek_be.mini_projek.dto.DiagnosaDTO;
import com.mini_projek_be.mini_projek.model.Diagnosa;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface DiagnosaService {

    Page<Diagnosa> getAllDiagnosa(String query, Long page);

    Diagnosa getDiagnosa(Long id);

    Diagnosa addDiagnosa(DiagnosaDTO diagnosa);

    Diagnosa editDiagnosa(Long id, DiagnosaDTO diagnosa);

    Map<String, Boolean> deleteDiagnosa(Long id);

    Page<Diagnosa> allDiangnosa(Long page, Long userId);
}
