package com.mini_projek_be.mini_projek.service;

import com.mini_projek_be.mini_projek.dto.PeriksaPasienDTO;
import com.mini_projek_be.mini_projek.dto.TanganiDTO;
import com.mini_projek_be.mini_projek.model.PeriksaPasien;
import org.springframework.data.domain.Page;

import java.util.List;

public interface PeriksaPasienService {

    Page<PeriksaPasien> allPasien(String query, Long page);

    PeriksaPasien addPasien(PeriksaPasienDTO periksaPasien);

    PeriksaPasien editTangani(Long id, TanganiDTO tanganiDto);

    PeriksaPasien getIdd(Long id);

    List<PeriksaPasien> filterTanggal(String query, String tanggal);

}
