package com.mini_projek_be.mini_projek.service;

import com.mini_projek_be.mini_projek.dto.TindakanDto;
import com.mini_projek_be.mini_projek.model.Tindakan;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface TindakanService {

    Page<Tindakan> getAllTindakan(String query, Long page);

    Tindakan getTindakan(Long id);

    Tindakan addTindakan(TindakanDto tindakan);

    Tindakan editTindakan(Long id, Tindakan tindakan);

    Map<String, Boolean> deleteTindakan(Long id);

    Page<Tindakan> allTindakan(Long page, Long userId);
}
