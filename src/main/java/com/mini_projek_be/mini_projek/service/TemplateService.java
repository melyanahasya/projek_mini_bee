package com.mini_projek_be.mini_projek.service;

import com.mini_projek_be.mini_projek.model.Pasien;
import com.mini_projek_be.mini_projek.model.TemplateGuru;
import com.mini_projek_be.mini_projek.repository.PasienRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.util.List;

@Service
public class TemplateService {

    @Autowired
    PasienRepository pasienRepository;

    public ByteArrayInputStream pExcell() {
        List<Pasien> pasiens = pasienRepository.findAll();
        ByteArrayInputStream in = TemplateGuru.templateToExcel(pasiens);
        return in;
    }
}
