package com.mini_projek_be.mini_projek.service;

import com.mini_projek_be.mini_projek.dto.ExcellDTO;
import com.mini_projek_be.mini_projek.model.ExcellGuru;
import com.mini_projek_be.mini_projek.model.Pasien;
import com.mini_projek_be.mini_projek.repository.PasienRepository;
import com.mini_projek_be.mini_projek.serviceRegister.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Service
public class ExcellGuruService {

    @Autowired
    PasienRepository pasienRepository;

    @Autowired
    RegisterService registerService;

    public ByteArrayInputStream loadGuru(ExcellDTO excellDTO, Long UserId) {
        List<Pasien> pasiens = pasienRepository.findAll();
        ByteArrayInputStream in = ExcellGuru.guruToExcell(pasiens);
        return in;
    }

    public void saveGuru(MultipartFile file, ExcellDTO excelDto) {
        try {
            List<Pasien> pasienList = ExcellGuru.excelToGuru(file.getInputStream());
            for (int i = 0 ; i < pasienList.size() -1 ; i ++ ) {
                pasienList.get(i).setSiswaId(registerService.getById(excelDto.getUserId()));
            }
            pasienRepository.saveAll(pasienList);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }

}
