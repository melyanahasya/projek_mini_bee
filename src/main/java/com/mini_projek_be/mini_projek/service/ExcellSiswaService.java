package com.mini_projek_be.mini_projek.service;

import com.mini_projek_be.mini_projek.dto.ExcellDTO;
import com.mini_projek_be.mini_projek.model.ExcellSiswa;
import com.mini_projek_be.mini_projek.model.Pasien;
import com.mini_projek_be.mini_projek.repository.PasienRepository;
import com.mini_projek_be.mini_projek.serviceRegister.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Service
public class ExcellSiswaService {

    @Autowired
    PasienRepository pasienRepository;

    @Autowired
    RegisterService registerService;

    public ByteArrayInputStream loadSiswa(ExcellDTO excellDTO, Long userId) {
        List<Pasien> pasiens = pasienRepository.getAllSiswa(userId);
        ByteArrayInputStream in = ExcellSiswa.siswaToExcell(pasiens);
        return in;
    }


    public void saveSiswa(MultipartFile file) {
        try {
            List<Pasien> pasiens = ExcellSiswa.excelToSiswa(file.getInputStream());
            pasienRepository.saveAll(pasiens);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }

    public ByteArrayInputStream pExcell() {
        List<Pasien> pasiens = pasienRepository.findAll();
        ByteArrayInputStream in = ExcellSiswa.siswaToExcell(pasiens);
        return in;
    }

}
