package com.mini_projek_be.mini_projek.service;

import com.mini_projek_be.mini_projek.dto.DaftarObatDTO;
import com.mini_projek_be.mini_projek.model.DaftarObat;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface DaftarObatService {

    Page<DaftarObat> getAllObat(String query, Long page);

    DaftarObat addObat(DaftarObatDTO daftarObat);

    DaftarObat editObat(Long id, DaftarObatDTO daftarObat);

    DaftarObat getIdObat(Long id);

    Map<String, Boolean> deleteObat(Long id);
}
