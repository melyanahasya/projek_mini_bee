package com.mini_projek_be.mini_projek.service;

import com.mini_projek_be.mini_projek.dto.ExcellDTO;
import com.mini_projek_be.mini_projek.model.ExcellKaryawan;
import com.mini_projek_be.mini_projek.model.Pasien;
import com.mini_projek_be.mini_projek.repository.PasienRepository;
import com.mini_projek_be.mini_projek.serviceRegister.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Service
public class ExcellKaryawanService {

    @Autowired
    PasienRepository pasienRepository;

    @Autowired
    RegisterService registerService;


    public ByteArrayInputStream loadKaryawan(ExcellDTO excellDTO, Long userId) {
        List<Pasien> pasiens = pasienRepository.getAllKaryawan(userId);
        ByteArrayInputStream in = ExcellKaryawan.karyawanToExcell(pasiens);
        return in;
    }


    public void saveKaryawan(MultipartFile file, ExcellDTO excellDTO) {
        try {
            List<Pasien> pasienList = ExcellKaryawan.excelToKaryawan(file.getInputStream());
            for (int i = 0 ; i < pasienList.size() -1 ; i ++ ) {
                pasienList.get(i).setSiswaId(registerService.getById(excellDTO.getUserId()));
            }

            pasienRepository.saveAll(pasienList);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }

    public ByteArrayInputStream pExcell() {
        List<Pasien> pasiens = pasienRepository.findAll();
        ByteArrayInputStream in = ExcellKaryawan.karyawanToExcell(pasiens);
        return in;
    }

}
