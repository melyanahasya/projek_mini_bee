package com.mini_projek_be.mini_projek.service;

import com.mini_projek_be.mini_projek.dto.PasienDTO;
import com.mini_projek_be.mini_projek.dto.PasienSiswaDTO;
import com.mini_projek_be.mini_projek.model.Pasien;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface PasienService {

    List<Pasien> allGuru (Long userId);

    List<Pasien> allSiswa (Long userId);

    List<Pasien> allKaryawan (Long userId);

    Page<Pasien> allSiswaId(Long page, Long userId);

    Pasien addPasien (PasienDTO pasienDto);

    Pasien addSiswa (PasienSiswaDTO pasienDto);

    Pasien addKaryawan (PasienDTO pasienDto);

    Pasien editPasien (Long id, PasienDTO pasienDto);

    Pasien editSiswa (Long id, PasienSiswaDTO pasienDto);

    Pasien editKaryawan (Long id, PasienDTO pasienDto);

    Pasien getId (Long id);

    Map<String, Boolean> deleteById (Long id);


}
