package com.mini_projek_be.mini_projek.service;

import com.mini_projek_be.mini_projek.dto.PenangananPertamaDTO;
import com.mini_projek_be.mini_projek.model.Diagnosa;
import com.mini_projek_be.mini_projek.model.PenangananPertama;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface PenangananPertamaService {

    Page<PenangananPertama> getAllPenanganan(String query, Long page);

    PenangananPertama getIdPenanganan(Long id);

    PenangananPertama addPenanganan(PenangananPertamaDTO penanganan);

    PenangananPertama editPenanganan(Long id, PenangananPertamaDTO penanganan);

    Map<String, Boolean> deletePenanganan(Long id);

    Page<PenangananPertama> allPenanganan(Long page, Long userId);

}
