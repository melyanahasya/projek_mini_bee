package com.mini_projek_be.mini_projek.dto;

public class PeriksaPasienDTO {

//    private Long namaGuru;
//
//    private Long namaSiswa;
//
//    private Long namaKaryawan;
//
//    private String keluhan;

    private Long namaPasien;

    private String StatusPasien;

    private String Status;

    private String keluhan;

    public Long getNamaPasien() {
        return namaPasien;
    }

    public void setNamaPasien(Long namaPasien) {
        this.namaPasien = namaPasien;
    }

    public String getStatusPasien() {
        return StatusPasien;
    }

    public void setStatusPasien(String statusPasien) {
        StatusPasien = statusPasien;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getKeluhan() {
        return keluhan;
    }

    public void setKeluhan(String keluhan) {
        this.keluhan = keluhan;
    }
}
