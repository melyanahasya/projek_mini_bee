package com.mini_projek_be.mini_projek.dto;

public class DaftarObatDTO {

    private String namaObat;

    private Long stok;

    public String getNamaObat() {
        return namaObat;
    }


    public void setNamaObat(String namaObat) {
        this.namaObat = namaObat;
    }

    public Long getStok() {
        return stok;
    }

    public void setStok(Long stok) {
        this.stok = stok;
    }
}
