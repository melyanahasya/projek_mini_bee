package com.mini_projek_be.mini_projek.dto;

public class ExcellDTO {

    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
