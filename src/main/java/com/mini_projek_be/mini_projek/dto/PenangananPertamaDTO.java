package com.mini_projek_be.mini_projek.dto;

public class PenangananPertamaDTO {

    private String namaPenangananPertama;

    public String getNamaPenangananPertama() {
        return namaPenangananPertama;
    }

    public void setNamaPenangananPertama(String namaPenangananPertama) {
        this.namaPenangananPertama = namaPenangananPertama;
    }
}
