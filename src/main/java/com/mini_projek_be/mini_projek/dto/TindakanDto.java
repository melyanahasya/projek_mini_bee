package com.mini_projek_be.mini_projek.dto;

public class TindakanDto {

    private String namaTindakan;

    public String getNamaTindakan() {
        return namaTindakan;
    }

    public void setNamaTindakan(String namaTindakan) {
        this.namaTindakan = namaTindakan;
    }
}
