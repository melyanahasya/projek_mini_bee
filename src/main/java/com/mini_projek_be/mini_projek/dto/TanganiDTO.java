package com.mini_projek_be.mini_projek.dto;

public class TanganiDTO {

    private String status;

    private Long panyakitId;

    private Long tindakanId;

    private Long penangananId;

    private String keluhan;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getPanyakitId() {
        return panyakitId;
    }

    public void setPanyakitId(Long panyakitId) {
        this.panyakitId = panyakitId;
    }

    public Long getTindakanId() {
        return tindakanId;
    }

    public void setTindakanId(Long tindakanId) {
        this.tindakanId = tindakanId;
    }

    public Long getPenangananId() {
        return penangananId;
    }

    public void setPenangananId(Long penangananId) {
        this.penangananId = penangananId;
    }

    public String getKeluhan() {
        return keluhan;
    }

    public void setKeluhan(String keluhan) {
        this.keluhan = keluhan;
    }
}
