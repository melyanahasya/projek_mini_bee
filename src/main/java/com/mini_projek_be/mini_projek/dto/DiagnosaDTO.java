package com.mini_projek_be.mini_projek.dto;

public class DiagnosaDTO {

    private String namaDiagnosa;

    public String getNamaDiagnosa() {
        return namaDiagnosa;
    }

    public void setNamaDiagnosa(String namaDiagnosa) {
        this.namaDiagnosa = namaDiagnosa;
    }
}
