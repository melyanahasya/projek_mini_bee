package com.mini_projek_be.mini_projek.dto;

import java.util.Date;

public class PasienKaryawanDTO {


    private String namaKaryawan;

    private String jabatan;

    private String tanggalLahirKaryawan;

    private String tempatKaryawan;

    private Date waktuKaryawan;

    private Long userId;

    public String getNamaKaryawan() {
        return namaKaryawan;
    }

    public void setNamaKaryawan(String namaKaryawan) {
        this.namaKaryawan = namaKaryawan;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getTanggalLahirKaryawan() {
        return tanggalLahirKaryawan;
    }

    public void setTanggalLahirKaryawan(String tanggalLahirKaryawan) {
        this.tanggalLahirKaryawan = tanggalLahirKaryawan;
    }

    public String getTempatKaryawan() {
        return tempatKaryawan;
    }

    public void setTempatKaryawan(String tempatKaryawan) {
        this.tempatKaryawan = tempatKaryawan;
    }

    public Date getWaktuKaryawan() {
        return waktuKaryawan;
    }

    public void setWaktuKaryawan(Date waktuKaryawan) {
        this.waktuKaryawan = waktuKaryawan;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
